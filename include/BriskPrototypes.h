/*
 * Copyright 2013 Li-Yuchong <93@liyc.me>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR AND CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef __BRISK_PROTOTYPES_H__
#define __BRISK_PROTOTYPES_H__

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <BRISK/include/Compiler.h>
#include <BRISK/include/BriskMemory.h>
#include <BRISK/include/BriskTypes.h>
#include <BRISK/include/BriskImage.h>
#include <BRISK/include/SimpleVector.h>

#include <BRISK/3rd-party/agast/oast9_16.h>
#include <BRISK/3rd-party/agast/agast5_8.h>

#define BRISK_BASIC_SIZE 12.0f

/* brisk structures & containers definition */

struct BriskPatternPoint
{
    b_float_t x;         /* x coordinate relative to center */
    b_float_t y;         /* y coordinate relative to center */
    b_float_t sigma;     /* Gaussian smoothing sigma */
};

struct BriskShortPair
{
    b_size_t i;          /* index of the first pattern point */
    b_size_t j;          /* index of other pattern point */
};

struct BriskLongPair
{
    b_size_t i;          /* index of the first pattern point */
    b_size_t j;          /* index of other pattern point */
    b_int_t weightedDx;  /* 1024.0/dx */
    b_int_t weightedDy;  /* 1024.0/dy */
};

typedef struct BriskPoint
{
    b_int_t x;
    b_int_t y;
} BriskPoint;

typedef struct FeaturePoint
{
    b_float_t x;
    b_float_t y;
    b_float_t size;
    b_float_t angle;
    b_float_t response;
    b_int_t   octave;
    
    b_uint32_t *descriptor;
    
} FeaturePoint;

SIMPLE_VECTOR_DECLARE(FeaturePointContainer, FeaturePoint);
SIMPLE_VECTOR_DECLARE(BriskLayerContainer, struct BriskLayer);
SIMPLE_VECTOR_DECLARE(IntVector, b_int_t);
SIMPLE_VECTOR_DECLARE(FloatVector, b_float_t);

enum _BriskCommonParams
{
    LAYER_HALF_SAMPLE      = 0,
    LAYER_TWO_THIRD_SAMPLE = 1
};

struct BriskScaleSpace
{
    b_uint8_t layers;
    BriskLayerContainer *pyramid;
    
    b_uint8_t threshold;
    b_uint8_t safeThreshold;
};

struct BriskLayer
{
    BriskImage *image;
    BriskImage *scores;
    
    b_float_t scale;
    b_float_t offset;
};

struct BriskDescriptorExtractor
{
    struct BriskPatternPoint *patternPoints;
    b_int_t points;
    
    b_float_t *scaleList;
    b_uint32_t *sizeList;
    
    b_int_t strings;
    b_float_t distanceMax;
    b_float_t distanceMin;
    
    struct BriskShortPair *shortPairs;
    struct BriskLongPair *longPairs;
    
    b_uint_t noShortPairs;
    b_uint_t noLongPairs;
    
    bool rotationInvariance;
    bool scaleInvariance;
};

/* File: BriskMisc.c */
FeaturePoint featurePointMake(b_float_t x, b_float_t y, b_float_t size, b_float_t angle, b_float_t response, b_int_t octave);
INLINE void featurePointDestroy (struct FeaturePoint *featurePoint);
INLINE void featurePointsDestroy(FeaturePointContainer *featurePoints);

/* File: BriskScaleSpace.c */
struct BriskScaleSpace *briskScaleSpaceCreate(b_uint8_t octaves);
struct BriskScaleSpace *briskScaleSpaceDestroy(struct BriskScaleSpace *ptr);
struct BriskScaleSpace *briskConstructPyramid(struct BriskScaleSpace *space, const BriskImage *image);
FeaturePointContainer  *briskDetectFeaturePoints(struct BriskScaleSpace *space, const b_uint16_t threshold);

/* File: BriskLayer.c */
BriskPoint *getAgastPoints(struct BriskLayer *layer, b_uint8_t threshold, b_int_t *length);
INLINE b_uint8_t getAgastScore(struct BriskLayer *layer, b_int_t x, b_int_t y, b_uint8_t threshold);
INLINE b_uint8_t getAgastScore_5_8(struct BriskLayer *layer, b_int_t x, b_int_t y, b_uint8_t threshold);
INLINE b_uint8_t getAgastScore2(struct BriskLayer *layer, b_float_t xf, b_float_t yf, b_uint8_t threshold, b_float_t scale);
struct BriskLayer briskLayerCreate(BriskImage *image, b_float_t scale, b_float_t offset);
struct BriskLayer briskLayerDerive(const struct BriskLayer *layer, b_int_t mode);

/* File: LayerResampler.c */
BriskImage *briskLayerHalfSample(const BriskImage *source, BriskImage *destination);
BriskImage *briskLayerTwoThirdSample(const BriskImage *source, BriskImage *destination);

/* File: BriskDescriptorExtractor.c */
struct BriskDescriptorExtractor *briskDescriptorExtractorinit(struct BriskDescriptorExtractor *extractor, b_float_t patternScale);
struct BriskDescriptorExtractor *briskDescriptorExtractorCreate(b_float_t patternScale);
void briskDescriptorExtractorDestroy(struct BriskDescriptorExtractor *extractor);
FeaturePointContainer *briskExtractFeatures(struct BriskDescriptorExtractor *extractor,
                                            const BriskImage *image, FeaturePointContainer *featurePoint);

#endif /* __BRISK_PROTOTYPES_H__ */
